
from payUnit import payUnit

from flask import Flask, render_template, request
app = Flask(__name__)

payment = payUnit({
    "apiUsername": 'payunit_sand_*****',
    "apiPassword": 'c1968535-1322-40f1-abf2-*****',
    "api_key": '8fed05e7e34c97afc6332a121cc12d*****',
    "return_url": "http://127.0.0.1:3500/thanks",
    "notify_url": "",
    "mode": "test",
    "name": "",
    "description": "",
    "purchaseRef": "",
    "currency": "XAF",
    "transaction_id":  "123456789",
})


@app.route('/', methods=['GET', 'POST'])
def hello_world():
    if(request.method == "POST"):
       payment.makePayment(request.form['amount'])

    return render_template('index.html')


@app.route('/thanks', methods=['GET'])
def welcome():
    return render_template('thanks.html')


# main driver function
if __name__ == '__main__':
    app.run()
