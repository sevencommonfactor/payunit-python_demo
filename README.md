Paynit Test Sdk


-  Creating a Virtual Environment  
    sudo apt-get install python3-pip 
    sudo pip3 install virtualenv 

- Run virtual environment with the following command:
   sudo virtualenv venv

 - Activating the virtual environment with the following command
   source venv/bin/activate 


- Install the Requirements
  pip3 install -r requirements.txt

-Run Project
 export FLASK_APP=main.py
 flask run -p 3500